package org.jean.borrowerservice.repository;

import org.jean.borrowerservice.entities.Borrower;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BorrowerRepository extends JpaRepository<Borrower, Long> {
}
