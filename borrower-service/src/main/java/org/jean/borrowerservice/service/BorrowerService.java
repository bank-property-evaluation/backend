package org.jean.borrowerservice.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jean.borrowerservice.dto.BorrowerDto;
import org.jean.borrowerservice.dto.MainBorrowerDto;
import org.jean.borrowerservice.entities.Borrower;
import org.jean.borrowerservice.entities.MainBorrower;
import org.jean.borrowerservice.mapper.BorrowerMapper;
import org.jean.borrowerservice.mapper.MainBorrowerMapper;
import org.jean.borrowerservice.repository.BorrowerRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class BorrowerService {

    private final BorrowerRepository borrowerRepository;
    private final MainBorrowerMapper mainBorrowerMapper;
    private final BorrowerMapper borrowerMapper;

    @Transactional
    public <T extends Borrower> T saveBorrower(T borrower) {
        return borrowerRepository.save(borrower);
    }

    @Transactional
    public MainBorrowerDto saveMainBorrower(MainBorrowerDto mainBorrowerDTO) {
        MainBorrower mainBorrower = saveBorrower(mainBorrowerMapper.toEntity(mainBorrowerDTO));
        log.info("borrower save");
        return mainBorrowerMapper.toDto(mainBorrower);
    }

    public List<BorrowerDto> getAllBorrowers() {
        return borrowerRepository.findAll().stream().map(borrowerMapper::toDto).collect(Collectors.toList());
    }

    public Optional<BorrowerDto> getBorrowerById(Long id) {
        return Optional
                .ofNullable(borrowerMapper.toDto(borrowerRepository.findById(id)
                        .orElse(null)));
    }

    public void deleteBorrower(Long id) {
        borrowerRepository.deleteById(id);
    }
}
