package org.jean.borrowerservice.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jean.borrowerservice.dto.BorrowerDto;
import org.jean.borrowerservice.dto.MainBorrowerDto;
import org.jean.borrowerservice.service.BorrowerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/borrowers")
@RequiredArgsConstructor
@Slf4j
public class BorrowerController {

    private final BorrowerService borrowerService;

    @PostMapping("/main")
    public ResponseEntity<MainBorrowerDto> addMainBorrower(@RequestBody MainBorrowerDto mainBorrowerDTO) {
        log.info("---------- addMainBorrower -------------");
        MainBorrowerDto savedMainBorrower = borrowerService.saveMainBorrower(mainBorrowerDTO);
        return ResponseEntity.ok(savedMainBorrower);
    }

    @GetMapping
    public ResponseEntity<List<BorrowerDto>> getAllBorrowers() {
        List<BorrowerDto> borrowers = borrowerService.getAllBorrowers();
        return ResponseEntity.ok(borrowers);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BorrowerDto> getBorrowerById(@PathVariable Long id) {
        return borrowerService.getBorrowerById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBorrower(@PathVariable Long id) {
        borrowerService.deleteBorrower(id);
        return ResponseEntity.noContent().build();
    }
}
