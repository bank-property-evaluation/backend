package org.jean.borrowerservice.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class JointBorrowerDto extends BorrowerDto {
    public JointBorrowerDto(Long id,
                            String customerNumber,
                            String customerName,
                            String contactNumber,
                            String email,
                            String address) {
        super(id, customerNumber, customerName, contactNumber, email, address);
    }

    public JointBorrowerDto() {}
}
