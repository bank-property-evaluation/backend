package org.jean.borrowerservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class BorrowerDto {
    protected Long id;
    protected String customerNumber;
    protected String customerName;
    protected String contactNumber;
    protected String email;
    protected String address;
}
