package org.jean.borrowerservice.entities;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "borrower_type", discriminatorType = DiscriminatorType.STRING)
public abstract class Borrower {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    protected String customerNumber;
    protected String customerName;
    protected String contactNumber;
    protected String email;
    protected String address;
}
