package org.jean.borrowerservice.entities;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("MAIN")
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@SuperBuilder
public class MainBorrower extends Borrower {

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "mainBorrower")
    private List<JointBorrower> jointBorrowers = new ArrayList<>();
}
