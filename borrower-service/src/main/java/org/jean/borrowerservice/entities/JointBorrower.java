package org.jean.borrowerservice.entities;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Entity
@DiscriminatorValue("JOINT")
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@SuperBuilder
public class JointBorrower extends Borrower {

    @ManyToOne
    @JoinColumn(name = "main_borrower_id")
    private MainBorrower mainBorrower;
}
