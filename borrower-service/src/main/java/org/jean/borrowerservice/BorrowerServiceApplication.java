package org.jean.borrowerservice;

import org.jean.borrowerservice.entities.JointBorrower;
import org.jean.borrowerservice.entities.MainBorrower;
import org.jean.borrowerservice.service.BorrowerService;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class BorrowerServiceApplication {

	@Bean
	public ModelMapper modelMapper(){
		return new ModelMapper();
	}

	public static void main(String[] args) {
		SpringApplication.run(BorrowerServiceApplication.class, args);
	}

	@Bean
	public CommandLineRunner dataLoader(BorrowerService borrowerService) {
		return args -> {
			JointBorrower jointBorrower1 = JointBorrower.builder()
					.customerNumber("J001")
					.customerName("Joint Borrower 1")
					.contactNumber("123456789")
					.email("joint1@example.com")
					.address("123 Joint St")
					.build();

			JointBorrower jointBorrower2 = JointBorrower.builder()
					.customerNumber("J002")
					.customerName("Joint Borrower 2")
					.contactNumber("987654321")
					.email("joint2@example.com")
					.address("456 Joint St")
					.build();

			MainBorrower mainBorrower1 = MainBorrower.builder()
					.customerNumber("M001")
					.customerName("Main Borrower 1")
					.contactNumber("111222333")
					.email("main1@example.com")
					.address("789 Main St")
					.build();

			jointBorrower1.setMainBorrower(mainBorrower1);
			jointBorrower2.setMainBorrower(mainBorrower1);
			mainBorrower1.setJointBorrowers(Arrays.asList(jointBorrower1, jointBorrower2));

			borrowerService.saveBorrower(mainBorrower1);
		};
	}

}
