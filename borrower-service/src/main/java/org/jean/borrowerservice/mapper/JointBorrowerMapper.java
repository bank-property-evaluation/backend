package org.jean.borrowerservice.mapper;

import org.jean.borrowerservice.dto.JointBorrowerDto;
import org.jean.borrowerservice.entities.JointBorrower;
import org.springframework.stereotype.Component;

import static java.util.Objects.isNull;

@Component
public class JointBorrowerMapper implements
        DtoMapperStrategy<JointBorrowerDto, JointBorrower>,
        EntityMapperStrategy<JointBorrower, JointBorrowerDto>{
    @Override
    public JointBorrowerDto toDto(JointBorrower jointBorrower) {
        if(isNull(jointBorrower)){
            return null;
        }
        return JointBorrowerDto
                .builder()
                .id(jointBorrower.getId())
                .customerNumber(jointBorrower.getCustomerNumber())
                .customerName(jointBorrower.getCustomerName())
                .contactNumber(jointBorrower.getContactNumber())
                .email(jointBorrower.getEmail())
                .address(jointBorrower.getAddress())
                .build();
    }

    @Override
    public JointBorrower toEntity(JointBorrowerDto jointBorrowerDTO) {
        if(isNull(jointBorrowerDTO)){
            return null;
        }
        return JointBorrower
                .builder()
                .id(jointBorrowerDTO.getId())
                .customerNumber(jointBorrowerDTO.getCustomerNumber())
                .customerName(jointBorrowerDTO.getCustomerName())
                .contactNumber(jointBorrowerDTO.getContactNumber())
                .email(jointBorrowerDTO.getEmail())
                .address(jointBorrowerDTO.getAddress()).build();
    }
}
