package org.jean.borrowerservice.mapper;


import org.jean.borrowerservice.dto.BorrowerDto;
import org.jean.borrowerservice.entities.Borrower;
import org.springframework.stereotype.Component;

import static java.util.Objects.isNull;

@Component
public class BorrowerMapper implements
        DtoMapperStrategy<BorrowerDto, Borrower>,
        EntityMapperStrategy<Borrower, BorrowerDto>{
    @Override
    public BorrowerDto toDto(Borrower borrower) {
        if(isNull(borrower)){
            return null;
        }
        return BorrowerDto
                .builder()
                .id(borrower.getId())
                .customerNumber(borrower.getCustomerNumber())
                .customerName(borrower.getCustomerName())
                .contactNumber(borrower.getContactNumber())
                .email(borrower.getEmail())
                .address(borrower.getAddress())
                .build();
    }

    @Override
    public Borrower toEntity(BorrowerDto borrowerDTO) {
        return null;
    }
}
