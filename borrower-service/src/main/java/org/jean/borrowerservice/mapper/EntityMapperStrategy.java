package org.jean.borrowerservice.mapper;

public interface EntityMapperStrategy<T,R> {
    T toEntity(R r);
}
