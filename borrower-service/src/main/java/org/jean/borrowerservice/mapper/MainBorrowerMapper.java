package org.jean.borrowerservice.mapper;

import lombok.RequiredArgsConstructor;
import org.jean.borrowerservice.dto.MainBorrowerDto;
import org.jean.borrowerservice.entities.MainBorrower;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Component
@RequiredArgsConstructor
public class MainBorrowerMapper implements
        DtoMapperStrategy<MainBorrowerDto, MainBorrower>,
        EntityMapperStrategy<MainBorrower, MainBorrowerDto>{

    private final JointBorrowerMapper jointBorrowerMapper;
    @Override
    public MainBorrowerDto toDto(MainBorrower mainBorrower) {
        if(isNull(mainBorrower)){
            return null;
        }
        return MainBorrowerDto
                .builder()
                .id(mainBorrower.getId())
                .customerNumber(mainBorrower.getCustomerNumber())
                .customerName(mainBorrower.getCustomerName())
                .contactNumber(mainBorrower.getContactNumber())
                .email(mainBorrower.getEmail())
                .address(mainBorrower.getAddress())
                .jointBorrowerDTOS(mainBorrower
                                .getJointBorrowers()
                                .stream()
                                .map(jointBorrowerMapper::toDto)
                                .collect(Collectors.toList()))
                .build();
    }

    @Override
    public MainBorrower toEntity(MainBorrowerDto mainBorrowerDTO) {
        if(isNull(mainBorrowerDTO)){
            return null;
        }
        return MainBorrower
                .builder()
                .id(mainBorrowerDTO.getId())
                .customerNumber(mainBorrowerDTO.getCustomerNumber())
                .customerName(mainBorrowerDTO.getCustomerName())
                .contactNumber(mainBorrowerDTO.getContactNumber())
                .email(mainBorrowerDTO.getEmail())
                .address(mainBorrowerDTO.getAddress())
                .jointBorrowers(mainBorrowerDTO
                        .getJointBorrowerDTOS()
                        .stream()
                        .map(jointBorrowerMapper::toEntity)
                        .collect(Collectors.toList()))
                .build();
    }
}
