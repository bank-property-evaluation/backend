package org.jean.borrowerservice.mapper;

public interface DtoMapperStrategy<T,R> {
    T toDto(R r);
}
