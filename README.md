# Bank Property Valuation Application

This project is a Bank Property Valuation Application developed using Java Spring Boot with a microservice architecture. The application allows bank officers to input and manage property valuation applications. It is designed to be secured, maintainable, and scalable.

## Microservices

The application consists of the following microservices:

1. **Config Server**
2. **Discovery Server**
3. **API Gateway**
4. **Property Valuation Service**
5. **Borrower Service**

## Microservice Modules

### Config Server
The Config Server is responsible for managing the configuration of all the microservices in a centralized manner.

### Discovery Server
The Discovery Server uses Netflix Eureka for service discovery. It enables each microservice to register itself and discover other services.

### API Gateway
The API Gateway uses Spring Cloud Gateway to route requests to the appropriate microservices. It also provides security, logging, and other cross-cutting concerns.

### Property Valuation Service
The Property Valuation Service manages the property valuation details and handles CRUD operations for property valuations.

### Borrower Service
The Borrower Service manages the borrower details and handles CRUD operations for borrowers.

## Prerequisites

- Java 17 or higher
- Maven 3.6.3 or higher

## Running the Application

To run the application, follow these steps:

1. **Clone the repository:**
   ```sh
   git clone https://gitlab.com/bank-property-evaluation/backend.git
   cd backend
