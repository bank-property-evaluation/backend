package org.jean.propertyvaluationservice.mapper;

import org.jean.propertyvaluationservice.dto.UserDto;
import org.jean.propertyvaluationservice.entities.User;
import org.springframework.stereotype.Component;

import static java.util.Objects.isNull;


@Component
public class UserMapper implements
        DtoMapperStrategy<UserDto, User>,
        EntityMapperStrategy<User, UserDto>{
    @Override
    public UserDto toDto(User user) {
        if(isNull(user)){
            return null;
        }
        return UserDto
                .builder()
                .id(user.getId())
                .username(user.getUsername())
                .contactNumber(user.getContactNumber())
                .businessUnit(user.getBusinessUnit())
                .initiatorName(user.getInitiatorName())
                .build();
    }

    @Override
    public User toEntity(UserDto userDto) {
        if(isNull(userDto)){
            return null;
        }
        return User
                .builder()
                .id(userDto.getId())
                .username(userDto.getUsername())
                .contactNumber(userDto.getContactNumber())
                .businessUnit(userDto.getBusinessUnit())
                .initiatorName(userDto.getInitiatorName())
                .build();
    }
}
