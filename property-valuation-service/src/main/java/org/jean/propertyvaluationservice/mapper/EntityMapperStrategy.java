package org.jean.propertyvaluationservice.mapper;

public interface EntityMapperStrategy<T,R> {
    T toEntity(R r);
}
