package org.jean.propertyvaluationservice.mapper;

public interface DtoMapperStrategy<T,R> {
    T toDto(R r);
}
