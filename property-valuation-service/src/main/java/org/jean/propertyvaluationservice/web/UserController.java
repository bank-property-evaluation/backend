package org.jean.propertyvaluationservice.web;

import lombok.RequiredArgsConstructor;
import org.jean.propertyevaluationservice.dto.UserDto;
import org.jean.propertyevaluationservice.service.UserService;
import org.jean.propertyevaluationservice.web.request.LoginDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/login")
    public ResponseEntity<UserDto> login(@RequestBody LoginDto loginDto) {
        // TODO: Handle it later, check user token etc ...
        UserDto existingUser = userService.findByUsername(loginDto.getUsername());
        if (existingUser != null && existingUser.getPassword().equals(loginDto.getPassword())) {
            return ResponseEntity.ok(existingUser);
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
}
