package org.jean.propertyvaluationservice.repository;

import org.jean.propertyvaluationservice.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
