package org.jean.propertyvaluationservice.service;

import lombok.RequiredArgsConstructor;
import org.jean.propertyvaluationservice.dto.UserDto;
import org.jean.propertyvaluationservice.mapper.UserMapper;
import org.jean.propertyvaluationservice.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserDto findByUsername(String username) {

        return userMapper.toDto(userRepository.findByUsername(username));
    }

    public UserDto save(UserDto userDto) {
        return userMapper.toDto(userRepository.save(userMapper.toEntity(userDto)));
    }
}
